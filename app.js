require ('colors');

const {inquirerMenu, pausa, leerInput, listadoDeTareasPorBorrar} = require('./helpers/inquirer');
const { guardarDB, leerDb } = require('./models/guardarArchivo');
const Tareas = require('./models/tareas');



const main = async() =>{
    
    let opt = '';
    const tareas = new Tareas();

    //Leer la base de datos
    const tareasDB = leerDb();

    if(tareasDB){
        tareas.cargarTareasFromArray(tareasDB);
    }
    
    //Ciclo del menu
    do{
        //Imprimir el menu
        opt = await inquirerMenu();
        
        switch(opt){
            case '1':
                const desc = await leerInput('Descripcion: ');
                tareas.crearTarea(desc)
                break;
            case '2':
                tareas.listarTarea();
                break
            case '3':
                tareas.listarTareaCompletada();
                break
            case '4':
                tareas.listarTareaCompletada(false);
                break
            case '5':
                //Completar tarea
                break
            case '6':
                const id = await listadoDeTareasPorBorrar(tareas.listadoArr);
                tareas.borrarTareas(id);
                break
        }

        //Esta funcion escribe la base de datos
        guardarDB(tareas.listadoArr);

        //Esta funcion da pausa al menu
        await pausa();
        
    }while(opt !== '0');

    
};

main();