const inquirer = require('inquirer');
require ('colors');

const preguntas = [
    {   
        type: 'list',
        name: 'opcion',
        message: `${'¿Qué desea hacer?'.red}`,
        choices: [
            
            {
                value: '1',
                name: `${'1.'.red} Crear tareas`,
            },
            {
                value: '2',
                name: `${'2.'.red} Listar tareas`,
            },
            {
                value: '3',
                name: `${'3.'.red} Listar tareas completadas`,
            },
            {
                value: '4',
                name: `${'4.'.red} Listar tareas pendientes`,
            },
            {
                value: '5',
                name: `${'5.'.red} Completar tarea(s)`,
            },
            {
                value: '6',
                name: `${'6.'.red} Borrar tarea`,
            },
            {
                value: '0',
                name: `${'0.'.red} Salir`,
            }
        ]
    },

];



const inquirerMenu = async() => {
    
    console.log('==============================='.america);
    console.log('     Seleccione una opción');
    console.log('===============================\n'.america);
    
    const {opcion} = await inquirer.prompt(preguntas);

    return opcion;
}


const pausa = async() =>{

    const question = [
        {
            type: 'input',
            name: 'enter',
            message: `\nPresione ${'ENTER'.green} para continuar\n`,
        }
    ]
    
    await inquirer.prompt(question); 
}

const leerInput = async(message) => {

    const question = [
        {
            type: 'input',
            name: 'desc',
            message,
            validate(value){
                if(value.length === 0){
                    return 'Por favor ingrese un valor';
                }
                return true;
            }
        }
    ] 

    const {desc} = await inquirer.prompt(question)
    return desc;
}

const listadoDeTareasPorBorrar = async(todasLasTareas) => {
    let choices = [];
    let cont = 0;

    todasLasTareas.forEach((tarea, i) => {
        const {desc, id} = tarea;
        cont += 1;
        
        let data = {
            value: id,
            name: `${i + 1}.`.blue + ' ' + desc,
        }
        
        choices[cont] = data;
    
    });
        
    
    let tareasListadas = [
        {   
            type: 'list',
            name: 'id',
            message: `${'¿Qué tareas desea borrar?'.red}`,
            choices
            
    }];

    const {id} = await inquirer.prompt(tareasListadas)

    return id;
}



module.exports = {
    inquirerMenu,
    pausa,
    leerInput,
    listadoDeTareasPorBorrar,
}