const Tarea = require("./tarea");

require ('colors');

class Tareas{

    constructor(){
        this._listado = {};
    }

    get listadoArr(){

        const listado = [];
        Object.keys(this._listado).forEach( key => {
            const tarea = this._listado[key];
            listado.push(tarea);
          }
            );

        return listado;
    }

    crearTarea(desc = ''){
        const tarea = new Tarea(desc)
        this._listado[tarea.id] = tarea;
    }

    cargarTareasFromArray(tareas = []){
            tareas.forEach( tarea => {
                this._listado[tarea.id] = tarea;
            })
    }

    listarTarea(){
        this.listadoArr.forEach((tarea, id) => {
            if(tarea.completadoEn != null){
                console.log(`${id + 1}.`.green, `${tarea.desc}`, '::', 'completada'.green)
            }else{
                console.log(`${id + 1}.`.red, `${tarea.desc}`, '::', 'pendiente'.red)
            }
        })
    }

    listarTareaCompletada(completadas = true){
        let contador = 0;
        this.listadoArr.forEach(tarea => {
            if(completadas){
                if(tarea.completadoEn){
                    contador += 1;
                    console.log(`${contador}.`.green, `${tarea.desc}`, '::', 'completada'.green)
                }
            }else{
                if(!tarea.completadoEn){
                    contador += 1;
                    console.log(`${contador}.`.red, `${tarea.desc}`, '::', 'pendiente'.red)
                    }
                }
            });
    }

    borrarTareas(id = ''){
        if(this._listado[id]){
            delete this._listado[id];
        }
    }

}

module.exports = Tareas;